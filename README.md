# Hi, I'm Debjit  <img src="https://github.com/wervlad/wervlad/assets/24524555/766d336d-b87d-44ba-807c-c51de2bc6b4d" width="28px" alt="👋" /> 



## About me

- 👨‍💻 I am currently studying B.Tech in CSE at Kalinga Institute Of Industrial Technology
- 📚 Every day I keep learning about Frontend and Backend technologies 😅
- 💪🏼 New objectives: Learn more and not stop developing ideas.
- ⚡ I love listening to music and read story books 🎼 📖.

---



### Contact me 📲


[<img align="left" alt="bilgehangecici | LinkedIn" width="50px" src="https://i.pinimg.com/originals/de/b4/6f/deb46f02a59e3b3a2aa58fac16290d63.gif" />][linkedin]
[<img align="left" alt="bilgehangecici | Instagram" width="50px" src="https://thumbs.gfycat.com/OrnateOrneryFoal-max-1mb.gif" />][instagram]
[<img align="left" alt="bilgehangecici | Facebook" width="50px" src="https://i.imgur.com/26xiPcn.gif" />][Facebook]
[<img align="left" alt="bilgehangecici | Twitter" width="50px" src="https://i.imgur.com/w42W6Bm.gif" />][Twitter]
[<img align="left" alt="bilgehangecici | Website" width="50px" src="https://i.imgur.com/jOPHjpU.gif" />][website]

<br />

---

### Skills 💻

  <img src="https://skillicons.dev/icons?i=linux,bsd,vim,git,github,gitlab,arduino,bash,c,cpp,css,deno,go,html,java,md,nim,nodejs,php,py,rust,scala,ts,zig,regex,django,express,flask,gtk,nextjs,nuxtjs,vue,qt,react,mongodb,mysql,postgres,redis,ansible,aws,cloudflare,docker,discord,ai,ps" alt="Skills">
<br/>

---


  <h2 align="center"> Github Statistics 📈 </h2>
  
  <div align="center"> 
     <a href="">
      <img align="center" src="http://github-profile-summary-cards.vercel.app/api/cards/profile-details?username=debjit-mandal&theme=highcontrast" />
    </a>
    <a href="">
      <img src="http://github-profile-summary-cards.vercel.app/api/cards/repos-per-language?username=debjit-mandal&theme=highcontrast"/>
    </a>
      <a href="">
      <img src="http://github-profile-summary-cards.vercel.app/api/cards/most-commit-language?username=debjit-mandal&theme=highcontrast"/>
    </a>
      <a href="">
      <img  src="http://github-profile-summary-cards.vercel.app/api/cards/stats?username=debjit-mandal&theme=highcontrast"/>
    </a>
      <a href="">
      <img src="http://github-profile-summary-cards.vercel.app/api/cards/productive-time?username=debjit-mandal&theme=highcontrast&utcOffset=8"/>
    </a>
</div
  
<br/>

---



[instagram]: https://www.instagram.com/iamdebjitmandal/
[linkedin]: https://www.linkedin.com/in/debjit-mandal-53676b22a/
[Facebook]: https://www.facebook.com/iamdebjitmandal
[Twitter]: https://twitter.com/iamdebjitmandal
[website]: https://debjit-dm.info
  
<h1 align="center">
  <a href="https://github.com/debjit-mandal/debjit-mandal/blob/main/debjit-mandalPubKey.gpg ">
  My gpg key!
  </a>
</h1>
  
---

  
